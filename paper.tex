%\def\final{}
%\def\print{}

\input{head/header}
\input{acronyms}

% Copyright
\newcommand{\CopyrightNotice}[2]{%
  \begin{picture}(0,0)(0,0)
    \put(#1){\parbox{\paperwidth-4em}{\sf \center {\small%
      This is the author's version of the work. The definitive work was
      published in \emph{Proceedings of the XXth International Conference on XYZ
      (ACRONYM), Place, Country, Date, Year.}}}}%
  \end{picture}
  \vspace{#2}
}

\begin{document}

\title{Title}

\author{
  Oliver Reiche, Frank Hannig, and J\"urgen Teich\\%
  Hardware/Software Co-Design, Department of Computer Science,\\
  Friedrich-Alexander University Erlangen-N\"urnberg (FAU), Germany.\\
  \texttt{\{oliver.reiche,hannig,teich\}@cs.fau.de}
  %authors omitted for blind review
}


\maketitle
%\CopyrightNotice{-40,160}{-2ex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Abstract
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
  \lipsum[1-2]
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Introduction
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}\label{sec:intro}
\lipsum[3-5]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Methods
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methods}\label{sec:methods}
This Section proposes useful guidelines for typesetting and using this template.

\subsection{Citations}
Either cite the reference id~\parencite{RSHMT14} only, the authors
\citeauthor{RSHMT14} only, or both~\textcite{RSHMT14} at once.

\subsection{References}
The type of reference is automatically resolved, such as \Cref{sec:methods},
\Cref{tab:example}, \Cref{fig:example}, \Cref{fig:subfig1,fig:subfig2},
\Cref{lst:hello}, and \Cref{alg:euclid}.

\subsection{Acronyms}
The first use of \ac{AOC} is written out followed by its abbreviation for later
use. Therefore, the second use of \ac{AOC} will only put the abbreviation.
Moreover, it is also possible to specify the use of plural if multiple
\acp{AOC} are mentioned in text.

\subsection{Lists}
Besides using \code{itemize} and \code{enumerate}, also \emph{inline} lists can
be defined. Advantages are:
\begin{enumerate*}[label=\upshape(\alph*),itemjoin={{; }},itemjoin*={{; and }}]
  \item Automatic generation of \emph{inline} numbering for each item
  \item insertion of separators between items
  \item placement of a different separator for the last item.
\end{enumerate*}
However, do not forget to put a full stop at the last item.

\subsection{Tables}
\begin{table}[t]
  \caption{A very simple example table.}
  \label{tab:example}
  \centering
  \scalebox{0.7}{
    \begin{tabular}{llrrrrrrr}
      \toprule
      Filter                    & Type   & II & ALUTs & Registers & LU (\%) & BRAM & DSP & Freq [MHz] \\
      \midrule
      \multirow{2}{*}{Gaussian} & normal &  2 &  8593 &     12423 &   19.86 &   84 &   4 &     132.50 \\
                                & fused  &  2 &  8439 &     11843 &   19.26 &   83 &   3 &     153.11 \\
      \cmidrule{2-9}
      \multirow{2}{*}{Sobel}    & normal &  2 &  8552 &     12433 &   19.76 &   84 &   4 &     131.25 \\
                                & fused  &  2 &  8230 &     12098 &   19.11 &   84 &   3 &     152.09 \\
      \bottomrule
    \end{tabular}
  }
\end{table}

Common rules for tables are to put captions above the table, to entirely omit
vertical lines, to put units in the table's header, to align numbers to the
right, and to always use the same amount of decimal places within a specific
column, as shown in \Cref{tab:example}.

\subsection{Figures}
Besides creating a separate floating environment for every Figure
(\Cref{fig:example}), multiple Figures can also be nicely grouped in sub
Figures, such as \Cref{fig:subfig1,fig:subfig2}.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.5\linewidth]{codesign}
  \caption{This is an example figure.}
  \label{fig:example}
\end{figure}

\begin{figure}[t]
  \centering
  \hfill
  \subfloat[First sub figure]{%
    \label{fig:subfig1}%
    \includegraphics[width=0.3\linewidth]{codesign}%
  }
  \hfill
  \subfloat[Second sub figure]{%
    \label{fig:subfig2}%
    \includegraphics[width=0.3\linewidth]{codesign}%
  }
  \hfill\null
  \caption{This is an example for sub figures.}
  \label{fig:subfig}
\end{figure}

\subsection{Listings}
A Listing can be put inline, like this

\lstinputlisting[%
  frame=none,%
  language=C++,%
  aboveskip=\medskipamount,%
  belowskip=\medskipamount]
  {code/helloworldshort.cpp}
which unfortunately cannot be referenced and could be split up into two on page
breaks. Alternatively, Listings could also be defined within a floating
environment, as provided in \Cref{lst:hello}.

\lstinputlisting[%
  float=t,%
  language=C++,%
  caption={Hello World example.},%
  label={lst:hello}]
  {code/helloworldfull.cpp}

It is also possible to include short code snippets like
\lstinline[language=C++]{std::vector<double>} in the text.

\subsection{Algorithms}
The equal sign (\code{=}) in pseudo code algorithms is used for testing
equality and not for assignments. For assignments, use \code{\textbackslash
gets} instead. Furthermore, variable names with more than one letter should be
set with \code{\textbackslash textrm}, as a name such as $var$ could be
interpret as $v\times a\times r$.

\begin{algorithm}
  \small
  \SetAlCapFnt{\small}
  \SetAlCapNameFnt{\small}

  \DontPrintSemicolon
  \SetKwProg{Func}{function}{}{end}
  \SetKwFunction{euclid}{Euclid}

  \caption{Euclid's algorithm}
  \label{alg:euclid}
  \Func(\tcp*[f]{The g.c.d. of a and b}){\euclid{$a,b$}}{
    $r \gets a\bmod b$\;
    \While(\tcp*[f]{We have the answer if r is 0}){$r\not=0$}{
      $a\gets b$\;
      $b\gets r$\;
      $r\gets a\bmod b$;\
    }
    \Return $b$ \tcp*{The gcd is b}
  }
\end{algorithm}

\subsection{Plots}
Plots should be defined as separate TikZ files. Data for plots should always be
provided in a separate data file. Rich examples for throughput, speedup, and
scatter plots are given in \Cref{fig:throughput,fig:speedup,fig:scatter}.

\begin{figure}[t]
  \centering
  \input{figures/throughput.tikz}
  \caption{Example for throughput plot.}
  \label{fig:throughput}
\end{figure}

\begin{figure}[t]
  \centering
  \input{figures/speedup.tikz}
  \caption{Example for speedup plot.}
  \label{fig:speedup}
\end{figure}

\begin{figure}[t]
  \centering
  \input{figures/scatter.tikz}
  \caption{Example for scatter plot.}
  \label{fig:scatter}
\end{figure}

\subsection{Todos}
Todos\todo{This is a todo} can simply be set everywhere in text. They will
automatically be hidden in \code{final} and \code{print} mode.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Results
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Evaluation and Results}\label{sec:results}
\lipsum[6]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Discussion
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}\label{sec:discussion}
\lipsum[7]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Related Work
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Work}\label{sec:related}
\lipsum[8]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Conclusion
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}\label{sec:conclusion}
\lipsum[9]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Acknowledgments
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Acknowledgment}
This work is supported by the German Research Foundation (DFG), as part of the
Research Training Group 1773 ``Heterogeneous Image Systems''.
The \tesla\,K20 used for this research was donated by the \nvidia Corporation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Bibliography
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\printbibliography


\end{document}

